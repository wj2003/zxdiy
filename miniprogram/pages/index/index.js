//index.js
const app = getApp()
const db = wx.cloud.database()

Page({
  data: {

  },

  onLoad: function() {
    app.globalData.diyEdit = "false";
    wx.cloud.callFunction({
      name: 'getOpenid',
      complete: res => {
        console.log('openid: ', res.result.openid);
        if (res.result.openid == app.config.adminOpenID){
          app.globalData.isAdmin = "true";  //说明是管理员
        }
        app.globalData.isIndex="true";
        db.collection('index_diy').get().then(res => {
          if (res.data.length > 0) {
            //value 2表示默认跳转轻站页面 1表示默认跳转商城页面
            app.globalData.diyId = res.data[0]._id;
            if (res.data[0].product == "1") {
              app.globalData.diyProduct = "1";
              wx.switchTab({
                url: `/pages/com/index/index`
              });
            } else if (res.data[0].product == "2") {
              app.globalData.diyProduct = "2";
              wx.redirectTo({
                url: `/pages/com/home/index`
              });
            }
          } else {
            //首次跳转  默认页面
            wx.redirectTo({
              url: `/pages/com/home/index`
            });
          }
        });
      }
    })
    
  },
})