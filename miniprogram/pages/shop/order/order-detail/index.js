// pages/order/order-detail/index.js
const util = require('../../../../utils/util.js')
const app = getApp();
const db = wx.cloud.database()
const _ = db.command
Page({
  data: {
    color: app.ext.color
  },
  onLoad(params) {
    console.log(params)
    if (params.order_id) {
      this.setData({
        order_id: params.order_id
      })
    }
  },
  onShow() {
    this.getOrderDetail();
  },
  getOrderDetail() {
    console.log(this.data.order_id)
    db.collection('order_info').where({
      _id: _.eq(this.data.order_id)
    }).get().then(res => {
      const order_info = res.data[0];
      this.setData({
        order_info: order_info
      })
    })
  },
  // 确认收货
  confirm_receipt(e) {
    const order_id = e.detail.value.order_id;
    const that = this;
    wx.showModal({
      title: "提示",
      content: "确定收货吗？",
      success: function (res) {
        if (res.confirm) {

          wx.cloud.callFunction({
            // 云函数名称
            name: 'order-update',
            // 传给云函数的参数
            data: {
              doit: 'qrsh',
              id: order_id,
              diyId: wx.getStorageSync('diyId')
            },
            complete: res => {
              console.log('callFunction test result: ', res)
              if (res.errMsg == "cloud.callFunction:ok") {
                    wx.showToast({
                title: "确认收货成功"
              });
                that.data.order_id = order_id;
                that.getOrderDetail();
              } else {
                wx.showModal({
                  title: "提示",
                  content: res.errmsg ? res.errmsg : "参数有误",
                  showCancel: false
                });
              }

            }
          })
        } else if (res.cancel) {
          console.log("用户点击取消");
        }
      }
    });
  },
  // 查询快递
  logistics(e) {
    const order = e.currentTarget.dataset.data;
    wx.navigateTo({
      url: `../logistics/index?order=${JSON.stringify(order)}`
    });
  },
  // 付款
  pay_order(e) {
    // payment_id ： 0余额 1微信 2支付宝 3货到
    // 微信支付
    this.data.order_id = e.currentTarget.dataset.id;
    // wx.doodoo.fetch(`/app/pay/wxpay/index?id=${e.detail.value.trade_id}`).then(res => {
    //   if (res && res.data.errmsg == 'ok') {
    //     let response = res.data.data;
    //     let timeStamp = response.timeStamp;
    //     //可以支付
    //     wx.requestPayment({
    //       timeStamp: timeStamp.toString(),
    //       nonceStr: response.nonceStr,
    //       package: response.package,
    //       signType: response.signType,
    //       paySign: response.paySign,
    //       success: () => {
    //         this.send_tplmsg(response.package, e.currentTarget.dataset.id); // 支付成功，发送支付成功模板
    //         this.getOrderDetail();
    //       },
    //       fail: () => {
    //         this.getOrderDetail();
    //       }
    //     });
    //   } else {
    //     wx.showModal({
    //       title: "提示",
    //       content: res.data.data,
    //       showCancel: !1
    //     });
    //   }
    // });
  },
  // 取消订单
  cancelOrder: function (e) {
    const order_id = e.detail.value.order_id;
    const that = this;
    wx.showModal({
      title: "提示",
      content: "确定取消订单吗？",
      success: function (res) {
        if (res.confirm) {
          wx.cloud.callFunction({
            // 云函数名称
            name: 'order-update',
            // 传给云函数的参数
            data: {
              doit: 'qxdd',
              id: order_id,
              diyId: wx.getStorageSync('diyId')
            },
            complete: res => {
              console.log('callFunction test result: ', res)
              if (res.errMsg == "cloud.callFunction:ok") {
                that.data.order_id = order_id;
                that.getOrderDetail();
              } else {
                wx.showModal({
                  title: "提示",
                  content: res.errmsg ? res.errmsg : "参数有误",
                  showCancel: false
                });
              }

            }
          })
        } else if (res.cancel) {
          console.log("用户点击取消");
        }
      }
    })
  },
  // 退款申请 申请售后
  backGoods(e) {
    const data = e.currentTarget.dataset.data;
    const type = e.currentTarget.dataset.type;
    console.log(data)
    console.log(type)
    console.log("tuikuan")
    wx.navigateTo({
      url: `../backGoods/index?data=${JSON.stringify(data)}&type=${type}`
    });
  },
  // 跳转到商品详情
  goProduct(e) {
    const product_id = e.currentTarget.dataset.product_id;
    wx.navigateTo({
      url: `/pages/shop/product/product-detail/index?id=${product_id}`
    });
  },
  // 发送模板消息
  send_tplmsg: function (formId, order_id) {
    formId = formId.substring(10);
    // wx.doodoo.fetch("/api/order/sendPayTpl", {
    //   method: "GET",
    //   data: {
    //     formId: formId,
    //     orderId: order_id
    //   }
    // }, res => {
    //   console.log("发送模板消息", res);
    // });
  }
});