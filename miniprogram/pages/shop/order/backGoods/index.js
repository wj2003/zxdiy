// pages/backGoods/index.js
const app = getApp();
const db = wx.cloud.database()
const _ = db.command
Page({
  data: {
    color: app.ext.color,
    showReason: false,
    backReason: [{name:'质量问题',index:1},{name:'物流问题',index:2}],
    selectItem:[],
    Q_description: '',  // 问题描述
    reasonIndex: -1 ,  // 退货原因选项
    totalPrice:0,
    reason:'',
    service_id:''
  },
  onLoad(option) {
    if (option.data) {
      wx.setNavigationBarTitle({
        title: option.type == 0 ? '退款申请' : '申请售后'
      });
      this.setData({
        type: option.type,
        backData: JSON.parse(option.data)
      })
      let tmp = 0;
      for(var i=0;i<this.data.backData.sp.length;i++){
        tmp = Number.parseInt(this.data.backData.sp[i].price)+tmp;
      }
      this.setData({
        totalPrice:tmp
      })
    }
  },
  onShow() {
    this.getBackReason();
  },
  getBackReason() {
    // wx.doodoo.fetch(`/shop/api/shop/order/service/index?type=${this.data.type}`).then(res => {
    //   if (res && res.data.errmsg == 'ok') {
    //     this.setData({
    //       backReason: res.data.data
    //     })
    //   }
    // })
  },
  bindinput(e) {
    this.setData({
      Q_description: e.detail.value,
      Q_length: e.detail.cursor
    })
  },
  hideModal() {
    let animation = wx.createAnimation({
      duration: 200,
      timingFunction: 'linear'
    })
    animation.translateY(0).step();
    this.setData({
      animationData: animation.export()
    })
    setTimeout(() => {
      animation.translateY(-335).step();
      this.setData({
        animationData: animation.export(),
        showSku: false
      })
    }, 200)
  },
  showModal(e) {
    // 0：加入购物车  1：立即购买
    let animation = wx.createAnimation({
      duration: 200, // 动画持续时间
      timingFunction: 'linear' // 定义动画效果，当前是匀速
    })
    animation.translateY(0).step();
    this.setData({
      animationData: animation.export(), // 通过export()方法导出数据
      showSku: true
    })
    // 设置setTimeout来改变y轴偏移量，实现有感觉的滑动
    setTimeout(() => {
      animation.translateY(-335).step();
      this.setData({
        animationData: animation.export()
      })
    }, 200)
  },
  submit() {
    if (this.data.reasonIndex < 0) {
      wx.showModal({
        title: '提示',
        content: '请选择退货原因',
        showCancel: false
      })
      return;
    }
    wx.cloud.callFunction({
      // 云函数名称
      name: 'order-update',
      // 传给云函数的参数
      data: {
        doit: 'tk',
        id: this.data.backData._id,
        diyId:wx.getStorageSync('diyId'),
        tkyy:this.data.reason,
        tkje:this.data.totalPrice,
        tkwtms: this.data.Q_description
      },
      complete: res => {
        console.log('callFunction test result: ', res)
        if (res.errMsg == "cloud.callFunction:ok") {
          wx.showToast({
            title: "申请退款成功"
          });
          wx.navigateBack();
        } else {
          wx.showModal({
            title: "提示",
            content: res.errmsg ? res.errmsg : "参数有误",
            showCancel: false
          });
        }

      }
    })

  },
  goDetail() {
    wx.navigateTo({
      url: `/pages/shop/product/product-detail/index?id=${this.data.backData.id}`
    });
  },
  selectReason(e) {
    const data = e.currentTarget.dataset.data;
    const reasonIndex = e.currentTarget.dataset.index;
    console.log(data)
    this.setData({
      selectItem: data,
      reason: data.name,
      reasonIndex: reasonIndex
    })
    console.log(this.data.reason)
  },
  chooseReason(e) {
    this.hideModal();
  }
})